TMX Market Centre is carving space in the core of Toronto's Financial District; the heart of Canada's Capital Markets. Opening 2020, this new construction will exceed 9000 square feet and feature a modern, versatile, event space for conference rental, meeting room bookings, team-building events, or corporate events.

Website: https://marketcentre.tmx.com